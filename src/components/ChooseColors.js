export default function ChooseColors({ sendColor, valueColor, addList, onList, listValueColor }) {


    // console.log('LOG: ChooseColor ', valueColor)


    return (
        <div>
            <legend>กดปุ่มเปลี่ยนสี:</legend>
            <div>
                <form onChange={sendColor} onClick={onList}>
                    <input type={"radio"} id={"red"} name={"choose"} value={"red"} />สีแดง
                    <input type={"radio"} id={"green"} name={"choose"} value={"green"} />สีเขียว
                    <input type={"radio"} id={"blue"} name={"choose"} value={"blue"} />สีน้ำเงิน
                </form>
            </div>
        </div >
    );
} 