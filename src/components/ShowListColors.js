export default function ShowListColors({ listColor }) {

    const onShow = listColor.map((showNewList, id) =>
        <li key={id}>{showNewList}</li>
    )


    return (
        <div>
            <fieldset>
                <legend>ข้อมูลที่เปลี่ยน:</legend>
                <ol id='text1'>{onShow}
                </ol>
            </fieldset>
        </div>
    )
}