import './App.css'
import ChooseColors from './components/ChooseColors'
import ChangeColor from './components/ChangeColor'
import ShowListColors from './components/ShowListColors'
import { useState } from 'react'

export default function App() {
    const [colors, setColors] = useState('')
    const [listColor, setList] = useState([])
    const [switchColor, setSwitchColor] = useState('')

    // NOTE: input colors
    const sendColor = (event) => {
        setColors(event.target.value)
    }

    // NOTE: Add list colors
    const addList = (event) => {
        setList(event.target.value)
    }
    const onList = (event) => {
        setList([...listColor, event.target.value,])
    }

    // NOTE: Switch color
    const sendSwitch = (event) => {
        setSwitchColor(...switchColor, listColor, event.value.target)
    }
    const onSwitchToHea = () => {
        setSwitchColor(...switchColor, colors)
    }


    return (
        <div>
            <ChooseColors sendColor={sendColor} valueColor={colors} addList={addList} listValueColor={listColor} onList={onList} sendSwitch={sendSwitch} ></ChooseColors>
            <ChangeColor valueColor={colors} onSwitchToHea={onSwitchToHea}></ChangeColor>
            <ShowListColors listColor={listColor} switchValueColor={switchColor}></ShowListColors>
        </div>
    )
}